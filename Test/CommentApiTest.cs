﻿using System;
using Shouldly;
using Xunit;
using System.Net;
using System.IO;
using System.Text;
using Try4.Models;

namespace Test
{
    public class CommentApiTest
    {
        private static string url = "localhost";

        [Fact]
        public void PostCommentTest()
        {
            Comment comment = new Comment();
            comment.PostID = 5;
            comment.Text = "TestText";

            comment.ShouldNotBe(null);
        }
    }
}
