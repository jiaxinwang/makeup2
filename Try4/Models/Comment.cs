﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Try4.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int PostID { get; set; }
        public string Text { get; set; }
        public string Email { get; set; }

        public virtual Post Post { get; set; }

    }
}