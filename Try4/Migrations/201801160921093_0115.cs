namespace Try4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0115 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comment", "Email", c => c.String());
            AddColumn("dbo.Post", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Post", "Email");
            DropColumn("dbo.Comment", "Email");
        }
    }
}
