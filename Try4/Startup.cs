﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Try4.Startup))]
namespace Try4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
