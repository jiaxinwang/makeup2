﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using Try4.Models;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private static string url = "http://localhost:60363/";

        [TestMethod]
        public void TestPostModel()
        {
            var post1 = new Post() { ID = 2, Detail = "Test Comment" };

            var post2 = post1;

            NUnit.Framework.Assert.That(post1, Is.EqualTo(post2));

        }
        [TestMethod]
        public void NullTestPass()
        {

            NUnit.Framework.Assert.That(null, Is.Null);
        }

    


        [TestMethod]
        public void AdsTypesAreNotEqual()
        {
            var ad1 = new Post
            {
                Item = "item1",
                Detail = "Test1"

            };

            var ad2 = new Post
            {
                Item = "item2",
                Detail = "Test2"

            };

            NUnit.Framework.Assert.AreNotEqual(ad1, ad2);
        }


        [TestMethod]
        public void AssertDifferentTypes()
        {
            var post = new Post();
            var comment = new Comment();
            NUnit.Framework.Assert.AreNotEqual(post.GetType(), comment.GetType());
        }

        [TestMethod]
        public void AssertPostSame()
        {
            var post1 = new Post();
            var post2 = new Post();
            NUnit.Framework.Assert.AreEqual(post1.GetType(), post2.GetType());
        }

        [TestMethod]
        public void AssertPostDoesNotContainPopCornProperty()
        {
            var post1 = new Post();
            var propertyList = post1.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            NUnit.Framework.Assert.False(flag);
        }

        [TestMethod]
        public void AssertCommentDoesContainTitleProperty()
        {
            var comment1 = new Comment();
            var propertyList = comment1.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "Title")
                {
                    flag = true;
                }
            }
            NUnit.Framework.Assert.False(flag);
        }
    }

}

